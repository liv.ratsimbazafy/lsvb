/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    images: {
        domains: ["www.proballers.com", "nm1.ffbb.com"],
    },
    // Prefer loading of ES Modules over CommonJS => for Swiper
    experimental: { esmExternals: true },
};

module.exports = nextConfig;
