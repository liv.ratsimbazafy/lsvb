export default `

#splash_screen{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh; 
    background: #fff;   
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    transition: all 2s cubic-bezier( 0.19, 1, 0.22, 1 );
    transform-origin: center;
    z-index: 1700;
}`;
