import { differenceInYears } from "date-fns";

export const calcAge = (birthDate) => {
    let [d, m, y] = birthDate.split(/\D/);
    y.substr(0, 1) === String(0) ? (y = String(20) + y) : (y = String(19) + y);
    return differenceInYears(new Date(), new Date(y, --m, d));
};

export function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}
