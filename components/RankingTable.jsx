import React from "react";
import Image from "next/image";

export default function RankingTable({ standing, heading }) {
    return (
        <section className="mt-5">
            <p className="text-sm text-primary dark:text-primaryLighter mb-10 leading-relaxed">
                {heading}
            </p>
            <div className="flex flex-col overflow-x-auto scrollbar-hide shadow-custom rounded-xl">
                <div className="sm:-mx-6 lg:-mx-8">
                    <div className="inline-block min-w-full sm:px-6 lg:px-8 bg-white dark:bg-primary_shade">
                        <div className="overflow-x-auto scrollbar-hide max-h-[340px]">
                            <table className="w-full text-sm text-left relative">
                                <thead className="text-primary uppercase bg-slate-100 dark:bg-primaryLight dark:text-primary sticky top-0">
                                    <tr>
                                        <th
                                            scope="col"
                                            className="px-2 py-3 rounded-tl-xl"
                                        >
                                            pos
                                        </th>
                                        <th
                                            scope="col"
                                            className="pl-0 pr-4 py-5"
                                        >
                                            equipe
                                        </th>
                                        <th
                                            scope="col"
                                            className="pl-6 pr-3 py-5"
                                        >
                                            pts
                                        </th>
                                        <th scope="col" className="pr-2 py-5">
                                            mj
                                        </th>
                                        <th scope="col" className="px-2 py-5">
                                            v
                                        </th>
                                        <th
                                            scope="col"
                                            className="px-2 py-5 rounded-tr-xl"
                                        >
                                            d
                                        </th>
                                    </tr>
                                </thead>
                                <tbody className="text-slate-400 dark:text-primaryLighter divide-y divide-slate-100 dark:divide-primary_shade">
                                    {standing.map((team, index) => {
                                        return (
                                            <tr
                                                key={index}
                                                className={`${
                                                    team.team.includes("Sables")
                                                        ? "bg-accent_blue dark:bg-accent dark:hover:bg-accentShade"
                                                        : "bg-white hover:bg-slate-50"
                                                } font-bold dark:bg-primary_shade dark:border-slate-300 dark:hover:bg-primary`}
                                            >
                                                <td
                                                    className={`${
                                                        team.team.includes(
                                                            "Sables"
                                                        ) && "text-white"
                                                    } w-4 p-4`}
                                                >
                                                    {team.position}
                                                </td>
                                                <th
                                                    scope="row"
                                                    className="flex items-center pl-0 pr-6 py-4 text-primary whitespace-nowrap dark:text-primaryLighter"
                                                >
                                                    <Image
                                                        className="w-8 h-8 rounded-full"
                                                        src={team.team_logo}
                                                        width={45}
                                                        height={45}
                                                        alt={
                                                            "logo équipe de " +
                                                            team.team
                                                        }
                                                    />
                                                    <div className="pl-3">
                                                        <div
                                                            className={`${
                                                                team.team.includes(
                                                                    "Sables"
                                                                ) &&
                                                                "text-white"
                                                            } text-sm font-semibold`}
                                                        >
                                                            {team.team}
                                                        </div>
                                                    </div>
                                                </th>
                                                <td
                                                    className={`${
                                                        team.team.includes(
                                                            "Sables"
                                                        ) && "text-white"
                                                    } pl-6 py-4 `}
                                                >
                                                    {team.points}
                                                </td>
                                                <td
                                                    className={`${
                                                        team.team.includes(
                                                            "Sables"
                                                        ) && "text-white"
                                                    } pr-2 py-4`}
                                                >
                                                    {team.total_game}
                                                </td>
                                                <td
                                                    className={`${
                                                        team.team.includes(
                                                            "Sables"
                                                        ) && "text-white"
                                                    } px-2 py-4`}
                                                >
                                                    {team.win}
                                                </td>
                                                <td
                                                    className={`${
                                                        team.team.includes(
                                                            "Sables"
                                                        ) && "text-white"
                                                    } px-2 py-4`}
                                                >
                                                    {team.loose}
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
