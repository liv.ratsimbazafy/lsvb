import React from "react";
import {
    PieChart,
    Pie,
    Legend,
    LabelList,
    Tooltip,
    ResponsiveContainer,
    Cell,
    XAxis,
} from "recharts";

const data01 = [
    { name: "Group A", value: 400 },
    { name: "Group B", value: 300 },
    { name: "Group C", value: 300 },
    { name: "Group D", value: 200 },
    { name: "Group E", value: 278 },
    { name: "Group F", value: 189 },
];

const data02 = [
    { name: "Victoire", value: 8 },
    { name: "Defaites", value: 18 },
];

const COLORS = ["#FF9F08", "#D8E7FC"];

export default function StatChart() {
    return (
        <div className="py-10 w-full h-[300px]">
            <ResponsiveContainer width="100%" height="100%">
                <PieChart width={120} height={120}>
                    <Pie
                        dataKey="value"
                        isAnimationActive={false}
                        data={data02}
                        cx="50%"
                        cy="50%"
                        outerRadius={60}
                        fill="#8884d8"
                        label={({
                            cx,
                            cy,
                            midAngle,
                            innerRadius,
                            outerRadius,
                            value,
                            index,
                        }) => {
                            //console.log("handling label?");
                            const RADIAN = Math.PI / 180;
                            // eslint-disable-next-line
                            const radius =
                                25 + innerRadius + (outerRadius - innerRadius);
                            // eslint-disable-next-line
                            const x =
                                cx + radius * Math.cos(-midAngle * RADIAN);
                            // eslint-disable-next-line
                            const y =
                                cy + radius * Math.sin(-midAngle * RADIAN);

                            return (
                                <text
                                    x={x}
                                    y={y}
                                    fill="#1D4B8B"
                                    textAnchor={x > cx ? "start" : "end"}
                                    dominantBaseline="central"
                                >
                                    {data02[index].name} - {value}
                                </text>
                            );
                        }}
                    >
                        {data01.map((entry, index) => (
                            <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                            />
                        ))}
                    </Pie>
                    {/* <Tooltip /> */}
                </PieChart>
            </ResponsiveContainer>
        </div>
    );
}
