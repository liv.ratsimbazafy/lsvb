import React, { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import Marquee from "./Marquee";
import { useTheme } from "next-themes";
import { motion } from "framer-motion";

// animate: defines animation
// initial: defines initial state of animation
//exit: defined when component exit

export default function Hero() {
    const { systemTheme, theme, setTheme } = useTheme();
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
    }, []);

    // const handleThemeChange = () => {
    //     if (!mounted) return null;
    //     const currentTheme = theme === "system" ? systemTheme : theme;
    //     return currentTheme === "dark" ? (
    //         <Image
    //             priority
    //             src="/static/images/dark-hero-img.png"
    //             alt="basket-ball player"
    //             width={750}
    //             height={500}
    //         />
    //     ) : (
    //         <Image
    //             priority
    //             // src="/static/images/hero-img.png"
    //             src="/static/images/dark-hero-img.png"
    //             alt="basket-ball player"
    //             width={750}
    //             height={500}
    //         />
    //     );
    // };
    const easing = [0.6, -0.05, 0.01, 0.99];
    const fade = {
        initial: {
            opacity: 0,
        },
        animate: {
            opacity: 1,
            transition: {
                duration: 2,
                ease: easing,
                delay: 0.5,
            },
        },
    };
    const fadeInUp = {
        initial: {
            y: 60,
            opacity: 0,
        },
        animate: {
            y: 0,
            opacity: 1,
            transition: {
                duration: 2,
                ease: easing,
                //delay: 0.3
            },
        },
    };
    const stagger = {
        animate: {
            transition: {
                staggerChildren: 0.1,
            },
        },
    };

    return (
        <motion.div initial="initial" animate="animate" exit={{ opacity: 0 }}>
            <section
                className={`min-h-[700px] bg-hero-bg dark:bg-dark-hero-bg bg-no-repeat bg-cover w-full overflow-hidden`}
            >
                <div className="container max-w-7xl grid grid-row-2 gap-y-6 pt-[3rem] pb-16 lg:pt-[12rem] lg:pb-[10rem] lg:grid-cols-2">
                    <motion.div
                        variants={stagger}
                        className="w-full self-center text-center order-2 lg:text-left lg:order-1"
                    >
                        <motion.div variants={fadeInUp}>
                            <span className="text-xl font-mulish text-primary font-black dark:text-white">
                                Saison 2022 - 2023
                            </span>
                        </motion.div>
                        <motion.div variants={fadeInUp}>
                            <h1 className="mx-auto text-4xl  max-w-md uppercase leading-tight tracking-tight mt-3 pb-[3rem] lg:mx-0 lg:text-6xl lg:max-w-2xl lg:leading-tight font-mulish font-black text-primary dark:text-white">
                                Ensemble pour ajuster le cap
                            </h1>
                        </motion.div>

                        <div className="flex justify-center items-center lg:justify-start gap-x-5 lg:gap-x-16 lg:mt-10">
                            <motion.div variants={fadeInUp}>
                                <Link
                                    href="#"
                                    aria-label="liens vers la billetterie"
                                    className="inline-block bg-accent_blue text-sm py-3 px-5 lg:py-4 lg:px-10 lg:text-md  text-white capitalize rounded-xl shadow-custom tracking-wide font-bold hover:bg-accentShade"
                                >
                                    billetterie
                                </Link>
                            </motion.div>
                            <motion.div variants={fadeInUp}>
                                <Link
                                    href="#"
                                    className="inline-block"
                                    aria-label="lien vers rejoindre le club"
                                >
                                    <div className="flex flex-row items-center py-2 text-primary_shade capitalize font-bold text-sm dark:text-white">
                                        <span className="bg-accent dark:bg-accent rounded-full">
                                            <svg
                                                width="44"
                                                height="44"
                                                viewBox="0 0 50 50"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <circle
                                                    cx="25"
                                                    cy="25"
                                                    r="25"
                                                    fill="transparent"
                                                />
                                                <path
                                                    d="M18.2084 25.5416H30.5626L26.7813 30.0833C26.6938 30.1886 26.6278 30.3102 26.5873 30.441C26.5467 30.5718 26.5323 30.7093 26.5449 30.8457C26.5702 31.1212 26.704 31.3753 26.9167 31.5521C27.1295 31.7289 27.4037 31.8139 27.6792 31.7885C27.9546 31.7631 28.2087 31.6294 28.3855 31.4166L33.5938 25.1666C33.6289 25.1169 33.6602 25.0647 33.6876 25.0104C33.6876 24.9583 33.7397 24.9271 33.7605 24.875C33.8077 24.7555 33.8324 24.6284 33.8334 24.5C33.8324 24.3716 33.8077 24.2444 33.7605 24.125C33.7605 24.0729 33.7084 24.0416 33.6876 23.9896C33.6602 23.9353 33.6289 23.883 33.5938 23.8333L28.3855 17.5833C28.2876 17.4657 28.1649 17.3712 28.0263 17.3064C27.8877 17.2415 27.7364 17.2081 27.5834 17.2083C27.34 17.2078 27.1042 17.2926 26.9167 17.4479C26.8113 17.5353 26.7241 17.6427 26.6602 17.7639C26.5963 17.8851 26.5569 18.0177 26.5443 18.1542C26.5317 18.2906 26.5462 18.4282 26.5869 18.559C26.6275 18.6899 26.6936 18.8114 26.7813 18.9166L30.5626 23.4583H18.2084C17.9321 23.4583 17.6672 23.5681 17.4718 23.7634C17.2765 23.9588 17.1667 24.2237 17.1667 24.5C17.1667 24.7762 17.2765 25.0412 17.4718 25.2366C17.6672 25.4319 17.9321 25.5416 18.2084 25.5416Z"
                                                    fill="#fff"
                                                />
                                            </svg>
                                        </span>
                                        <span className="ml-2 tracking-tight">
                                            rejoindre le club
                                        </span>
                                    </div>
                                </Link>
                            </motion.div>
                        </div>
                    </motion.div>

                    <motion.div
                        variants={fade}
                        className="w-full order-1 lg:order-2"
                    >
                        <Image
                            priority
                            src="/static/images/dark-hero-img.png"
                            alt="basket-ball player"
                            width={750}
                            height={650}
                        />
                    </motion.div>
                </div>
                <Marquee />
            </section>
        </motion.div>
    );
}
