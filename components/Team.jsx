import React, { useState } from "react";

import GlobalStats from "./GlobalStats";
import TeamLeaders from "./TeamLeaders";
import Roster from "./Roster";
import PlayerStats from "./PlayerStats";
import Link from "next/link";

export default function Team({
    leaders,
    roster,
    stats,
    players_data,
    columns,
}) {
    const [isStat, setIsStat] = useState(true);
    const [isLeaders, setIsLeaders] = useState(false);
    const [isRoster, setIsRoster] = useState(false);

    const handleTabs = (e) => {
        let tgt = e.currentTarget.dataset.name;
        switch (tgt) {
            case "roster":
                setIsRoster(true);
                setIsLeaders(false);
                setIsStat(false);
                break;
            case "leaders":
                setIsLeaders(true);
                setIsRoster(false);
                setIsStat(false);
                break;
            case "stats":
                setIsStat(true);
                setIsRoster(false);
                setIsLeaders(false);
                break;
            default:
                setIsLeaders(true);
                break;
        }
    };

    return (
        <section className="pt-[8rem]">
            <section className="container max-w-6xl px-5">
                <h3 className="text-2xl font-black font-mulish text-primary dark:text-primaryLighter uppercase">
                    #statistiques.
                </h3>

                {/* switch */}
                <div className="flex flex-row justify-start items-center gap-x-3 py-5 overflow-x-auto scrollbar-hide">
                    <button
                        className={`px-4 py-2.5 ${
                            isStat
                                ? "bg-accent_blue text-white"
                                : "border border-slate-300 text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                        } text-sm font-bold capitalize rounded-xl min-w-[115px]`}
                        onClick={handleTabs}
                        data-name="stats"
                    >
                        équipe
                    </button>
                    <button
                        className={`px-4 py-2.5 ${
                            isLeaders
                                ? "bg-accent_blue text-white"
                                : "border border-slate-300 text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                        } text-sm font-bold capitalize rounded-xl`}
                        onClick={handleTabs}
                        data-name="leaders"
                    >
                        joueurs
                    </button>
                    <Link href="/nm1">
                        <button
                            className={`px-4 py-2.5 ${
                                isRoster
                                    ? "bg-accent_blue text-white"
                                    : "border border-slate-300 text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                            } text-sm font-bold capitalize rounded-xl`}
                            onClick={handleTabs}
                            data-name="roster"
                        >
                            Effectif
                        </button>
                    </Link>
                </div>

                {/* team global stats */}
                {isStat && <GlobalStats stats={stats} />}

                {/* leaders section */}
                {/* {isLeaders && <TeamLeaders leaders={leaders} />} */}
                {isLeaders && (
                    <PlayerStats data={players_data} columns={columns} />
                )}

                {/* team roster */}
                {isRoster && <Roster roster={roster} />}
            </section>
        </section>
    );
}
