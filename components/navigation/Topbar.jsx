import React, { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import { useTheme } from "next-themes";
import { SunIcon, MoonIcon } from "@heroicons/react/solid";

const navLinks = [
    {
        href: "#",
        label: "le club",
        targetSeg: "home",
    },
    {
        href: "/nm1",
        label: "nm1",
        targetSeg: "nm1",
    },
    {
        href: "#",
        label: "equipes",
        targetSeg: "equipes",
    },
    {
        href: "/billetterie",
        label: "billetterie",
        targetSeg: "billetterie",
    },
    {
        href: "#",
        label: "boutique",
        targetSeg: "boutique",
    },
    {
        href: "#",
        label: "lsvb tv",
        targetSeg: "tv",
    },
];

const StyledLink = ({ href, label, currentRoute }) => {
    return (
        <Link
            href={href}
            aria-label={label}
            className={`mr-10 uppercase font-bold text-sm ${
                currentRoute === href ? "text-accent" : "text-primary"
            } dark:text-white hover:text-accent dark:hover:text-accent duration-300 dark:duration-300`}
        >
            {label}
        </Link>
    );
};

export default function Topbar({ isOpen, setIsOpen, currentRoute }) {
    const { systemTheme, theme, setTheme } = useTheme();
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
    }, []);

    const handleThemeChange = () => {
        if (!mounted) return null;
        const currentTheme = theme === "system" ? systemTheme : theme;
        return currentTheme === "dark" ? (
            <button
                role="button"
                aria-label="theme nuit"
                className="bg-white w-10 h-10 flex justify-center items-center rounded-full"
            >
                <SunIcon
                    className="w-5 h-5 text-primary duration-300"
                    role="button"
                    onClick={() => setTheme("light")}
                />
            </button>
        ) : (
            <button
                role="button"
                aria-label="theme jour"
                className="bg-primary w-10 h-10 flex justify-center items-center rounded-full"
            >
                <MoonIcon
                    className="w-5 h-5 text-white"
                    role="button"
                    onClick={() => setTheme("dark")}
                />
            </button>
        );
    };

    return (
        <div className="w-full fixed top-0 z-[999] lg:backdrop-blur">
            <nav className="container flex flex-row items-center justify-between px-5 py-2 xl:px-0 lg:py-7">
                {/* left links */}
                <div className="hidden lg:flex">
                    {navLinks.slice(0, 3).map((link, i) => (
                        <StyledLink
                            key={i}
                            href={link.href}
                            label={link.label}
                            className="mr-3 uppercase"
                            currentRoute={currentRoute}
                        />
                    ))}
                </div>
                {/* center logo */}
                <Link href="/" className="lg:hidden" aria-label="logo du site">
                    <Image
                        src="/logo.svg"
                        alt="website logo"
                        width={80}
                        height={80}
                    />
                </Link>

                <Link
                    href="/"
                    className="hidden lg:block fixed top-0 left-[50%] translate-x-[-50%] z-[100]"
                >
                    <Image
                        src="/logo.svg"
                        alt="website logo"
                        width={150}
                        height={150}
                    />
                </Link>

                <div className="flex flex-row items-center">
                    <div className="hidden lg:flex flex-row items-center">
                        {navLinks.slice(3, 6).map((link, i) => (
                            <StyledLink
                                key={i}
                                href={link.href}
                                label={link.label}
                                currentRoute={currentRoute}
                            />
                        ))}
                    </div>
                    <div className="invisible lg:visible">
                        {handleThemeChange()}
                    </div>
                    <div
                        className="lg:hidden w-12 h-12 mt-3 rounded-full flex justify-center items-center bg-white dark:bg-white shadow-xl"
                        onClick={() => setIsOpen(!isOpen)}
                    >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-6 h-6 text-primary"
                            role="button"
                        >
                            <path
                                fillRule="evenodd"
                                d="M3 6.75A.75.75 0 013.75 6h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 6.75zM3 12a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 12zm8.25 5.25a.75.75 0 01.75-.75h8.25a.75.75 0 010 1.5H12a.75.75 0 01-.75-.75z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </div>
                </div>
            </nav>
        </div>
    );
}
