import React from "react";
import {
    useTable,
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination,
} from "react-table";
import {
    ChevronLeftIcon,
    ChevronRightIcon,
    ChevronDoubleLeftIcon,
    ChevronDoubleRightIcon,
} from "@heroicons/react/solid";
import { Button, PageButton } from "./PaginationButton";

export default function PlayerStats({ data, columns }) {
    console.log(data);
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,

        //new
        page, // Instead of using 'rows', we'll use page,
        // which has only the rows for the active page

        // The rest of these things are super handy, too ;)
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,

        state,
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
        },
        useFilters,
        useGlobalFilter,
        useSortBy,
        usePagination // new
    );

    return (
        <div className="mt-4">
            <>
                {/* global search and filter */}
                {/* table */}

                <div className="overflow-x-auto scrollbar-hide shadow-xl rounded-xlbg-white">
                    <div className="align-middle inline-block min-w-full">
                        <div className="overflow-hidden rounded-xl">
                            <table {...getTableProps()} className="min-w-full">
                                <thead className="bg-slate-100 dark:bg-primaryLight">
                                    {headerGroups.map((headerGroup) => (
                                        <tr
                                            {...headerGroup.getHeaderGroupProps()}
                                        >
                                            {headerGroup.headers.map(
                                                (column) => (
                                                    // Add the sorting props to control sorting. For this example
                                                    // we can add them into the header props
                                                    <th
                                                        scope="col"
                                                        className="px-2 py-5 text-left text-sm text-primary uppercase"
                                                        {...column.getHeaderProps(
                                                            column.getSortByToggleProps()
                                                        )}
                                                    >
                                                        <div className="flex flex-row">
                                                            {column.render(
                                                                "Header"
                                                            )}
                                                            {/* Add a sort direction indicator */}
                                                            <span className="text-accent">
                                                                {column.isSorted
                                                                    ? column.isSortedDesc
                                                                        ? " ▼"
                                                                        : " ▲"
                                                                    : ""}
                                                            </span>
                                                        </div>
                                                    </th>
                                                )
                                            )}
                                        </tr>
                                    ))}
                                </thead>
                                <tbody
                                    {...getTableBodyProps()}
                                    className="bg-white divide-y divide-slate-100 dark:bg-primary_shade dark:divide-primary_shade"
                                >
                                    {page.map((row, i) => {
                                        // new
                                        prepareRow(row);
                                        return (
                                            <tr
                                                {...row.getRowProps()}
                                                className="text-sm hover:bg-slate-50 dark:hover:bg-primary"
                                            >
                                                {row.cells.map((cell) => {
                                                    //console.log(cell);
                                                    return (
                                                        <td
                                                            {...cell.getCellProps()}
                                                            //className="px-2 py-4 whitespace-nowrap"
                                                            className={`${
                                                                cell.column
                                                                    .Header ===
                                                                "Joueur"
                                                                    ? "text-primary dark:text-primaryLighter"
                                                                    : "text-slate-400 dark:text-primaryLighter"
                                                            } px-2 py-5 font-bold whitespace-nowrap`}
                                                        >
                                                            {cell.render(
                                                                "Cell"
                                                            )}
                                                        </td>
                                                    );
                                                })}
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {/* Pagination */}
                <div className="pt-10 flex items-center justify-between">
                    <div className="flex-1 flex justify-between sm:hidden">
                        <Button
                            onClick={() => previousPage()}
                            disabled={!canPreviousPage}
                        >
                            Précedent
                        </Button>
                        <Button
                            onClick={() => nextPage()}
                            disabled={!canNextPage}
                        >
                            Suivant
                        </Button>
                    </div>
                    <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                        <div className="flex items-center gap-x-2">
                            <span className="text-sm text-primaryLight">
                                Page{" "}
                                <span className="font-sm">
                                    {state.pageIndex + 1}
                                </span>{" "}
                                sur{" "}
                                <span className="font-sm">
                                    {pageOptions.length}
                                </span>
                            </span>
                            <label>
                                <span className="sr-only">Items Per Page</span>
                                <select
                                    className="mt-1 block w-full text-sm rounded-md border-slate-300 shadow-sm focus:border-slate-200 focus:ring focus:ring-slate-200 focus:ring-opacity-50 dark:bg-primary_shade dark:border-primary_shade"
                                    value={state.pageSize}
                                    onChange={(e) => {
                                        setPageSize(Number(e.target.value));
                                    }}
                                >
                                    {[3, 6, 12].map((pageSize) => (
                                        <option key={pageSize} value={pageSize}>
                                            Voir {pageSize}
                                        </option>
                                    ))}
                                </select>
                            </label>
                        </div>
                        <div>
                            <nav
                                className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px"
                                aria-label="Pagination"
                            >
                                <PageButton
                                    className="rounded-l-md"
                                    onClick={() => gotoPage(0)}
                                    disabled={!canPreviousPage}
                                >
                                    <span className="sr-only">First</span>
                                    <ChevronDoubleLeftIcon
                                        className="h-5 w-5"
                                        aria-hidden="true"
                                    />
                                </PageButton>
                                <PageButton
                                    onClick={() => previousPage()}
                                    disabled={!canPreviousPage}
                                >
                                    <span className="sr-only">Previous</span>
                                    <ChevronLeftIcon
                                        className="h-5 w-5"
                                        aria-hidden="true"
                                    />
                                </PageButton>
                                <PageButton
                                    onClick={() => nextPage()}
                                    disabled={!canNextPage}
                                >
                                    <span className="sr-only">Next</span>
                                    <ChevronRightIcon
                                        className="h-5 w-5"
                                        aria-hidden="true"
                                    />
                                </PageButton>
                                <PageButton
                                    className="rounded-r-md"
                                    onClick={() => gotoPage(pageCount - 1)}
                                    disabled={!canNextPage}
                                >
                                    <span className="sr-only">Last</span>
                                    <ChevronDoubleRightIcon
                                        className="h-5 w-5"
                                        aria-hidden="true"
                                    />
                                </PageButton>
                            </nav>
                        </div>
                    </div>
                </div>
            </>
        </div>
    );
}
