import React, { useState, useRef } from "react";
import Image from "next/image";
import { calcAge } from "@/helpers";

import { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

// Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

export default function Roster({ roster }) {
    const swiperRef = useRef();
    const [slideCount, setSlideCount] = useState(1);
    return (
        <div className="relative">
            <Swiper
                modules={[Navigation]}
                spaceBetween={20}
                breakpoints={{
                    320: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                    },
                    1024: {
                        slidesPerView: 3,
                        slidesPerGroup: 3,
                    },
                }}
                onSwiper={(swiper) => {
                    setSlideCount(1);
                }}
                onSlideChange={(swiper) =>
                    setSlideCount(swiper.activeIndex / 2 + 1)
                }
                onBeforeInit={(swiper) => {
                    swiperRef.current = swiper;
                }}
            >
                {roster.map((item, index) => {
                    return (
                        <SwiperSlide key={index}>
                            <div className="mt-12 text-center bg-white dark:bg-primary_shade border border-slate-200 dark:border-primary rounded-xl min-w-[140px] min-h-[150px] relative">
                                <Image
                                    blurDataURL="/static/svg/hoop.svg"
                                    src={item.avatar}
                                    alt={item.player}
                                    width={70}
                                    height={70}
                                    className="mx-auto absolute top-[-35px] left-[50%] translate-x-[-50%] rounded-full drop-shadow-xl"
                                />
                                <div className="pt-12 pb-3">
                                    <p className="uppercase text-primary dark:text-primaryLighter text-sm font-black">
                                        {item.player}
                                    </p>
                                    <p className="text-sm ext-primary dark:text-primaryLighter font-medium capitalize my-2">
                                        {item.post}
                                    </p>
                                    <p className="text-primaryLight text-sm mt-2">
                                        {item.height} -{" "}
                                        {calcAge(item.birth) + " ans"}
                                    </p>
                                </div>
                            </div>
                        </SwiperSlide>
                    );
                })}
            </Swiper>
            <p className="mx-auto text-center mt-8 p-2 rounded-full font-bold border border-slate-300 bg-white text-slate-400 dark:bg-primary_shade dark:text-primaryLighter dark:border-primary text-sm max-w-[80px]">
                {slideCount} / {roster.length / 2}
            </p>
        </div>
    );
}
