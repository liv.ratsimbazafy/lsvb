import React, { useState, useRef } from "react";
import Image from "next/image";
import Link from "next/link";
import ButtonLink from "./ButtonLink";
// Import Swiper React components
import { Navigation, Pagination, Scrollbar, Virtual } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { motion } from "framer-motion";

export default function Calendar({ games }) {
    const [lastGame, setLastGame] = useState(false);
    const [disableSwiperNext, setDisableSwiperNext] = useState(false);
    const [disableSwiperPrev, setDisableSwiperPrev] = useState(false);

    const swiperRef = useRef();
    const handleTabs = (e) => {
        const tgt = e.currentTarget.dataset.name;
        if (
            (tgt === "last_game" && lastGame) ||
            (tgt === "next_games" && !lastGame)
        )
            return;

        setLastGame(!lastGame);
    };

    return (
        <motion.div
            initial={{ opacity: 0 }}
            whileInView={{ opacity: 1, transition: { delay: 0.3 } }}
            className="pt-16 lg:pt-[10rem]"
        >
            <section className="container max-w-6xl px-5">
                <h3 className="text-2xl font-black font-mulish text-primary dark:text-primaryLighter uppercase">
                    #calendrier.
                </h3>

                {/* switch */}
                <div className="flex flex-row justify-start items-center gap-x-3 pt-5 pb-10">
                    <button
                        className={`p-3 ${
                            !lastGame
                                ? "bg-accent_blue text-primaryLighter"
                                : "border border-slate-300 text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                        } text-sm font-bold capitalize rounded-xl`}
                        onClick={handleTabs}
                        data-name="next_games"
                        aria-label="voir les matches à venir"
                    >
                        matchs à venir
                    </button>
                    <button
                        className={`p-3 ${
                            lastGame
                                ? "bg-accent_blue text-primaryLighter"
                                : "border border-slate-300 text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade "
                        } text-sm font-bold capitalize rounded-xl`}
                        onClick={handleTabs}
                        data-name="last_game"
                        role="presentation"
                        aria-label="voir le dernier résultat"
                    >
                        dernier résultats
                    </button>
                </div>

                {/* llasGame Card */}
                {lastGame && <LastGameCard games={games} />}

                {/* nextgame carousel*/}
                {!lastGame && (
                    <div className="relative">
                        <Swiper
                            className="bg-white dark:bg-primary rounded-3xl"
                            modules={[Navigation]}
                            spaceBetween={20}
                            breakpoints={{
                                640: {
                                    slidesPerView: 1,
                                },
                                768: {
                                    slidesPerView: 2,
                                },
                                1024: {
                                    slidesPerView: 3,
                                },
                            }}
                            onSwiper={(swiper) =>
                                swiper.activeIndex === 0 &&
                                setDisableSwiperPrev(true)
                            }
                            onSlideChange={(swiper) => {
                                swiper.isBeginning
                                    ? setDisableSwiperPrev(true)
                                    : setDisableSwiperPrev(false);
                                swiper.isEnd
                                    ? setDisableSwiperNext(true)
                                    : setDisableSwiperNext(false);
                            }}
                            onBeforeInit={(swiper) => {
                                swiperRef.current = swiper;
                            }}
                        >
                            {games.slice(1, 4).map((game, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <UpcomingGames
                                            game={game}
                                            index={index}
                                        />
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                        <div className="lg:hidden">
                            <button
                                onClick={() => swiperRef.current?.slidePrev()}
                                className={`rounded-full border border-slate-300 dark:border-primary ${
                                    disableSwiperPrev
                                        ? "bg-slate-100 invisible"
                                        : "border border-slate-300 bg-white visible"
                                } text-white absolute left-[-20px] top-2/4 translate-y-[-50%] z-50`}
                                disabled={disableSwiperPrev}
                            >
                                <svg
                                    width="40"
                                    height="40"
                                    viewBox="0 0 50 50"
                                    fill="none"
                                >
                                    <circle
                                        cx="25"
                                        cy="25"
                                        r="25"
                                        transform="matrix(-1 0 0 1 50 0)"
                                        fill="transparent"
                                    />
                                    <path
                                        d="M31.7916 25.5416H19.4374L23.2187 30.0833C23.3062 30.1886 23.3722 30.3102 23.4127 30.441C23.4533 30.5718 23.4677 30.7093 23.4551 30.8457C23.4298 31.1212 23.296 31.3753 23.0833 31.5521C22.8705 31.7289 22.5963 31.8139 22.3208 31.7885C22.0454 31.7631 21.7913 31.6294 21.6145 31.4166L16.4062 25.1666C16.3711 25.1169 16.3398 25.0647 16.3124 25.0104C16.3124 24.9583 16.2603 24.9271 16.2395 24.875C16.1923 24.7555 16.1676 24.6284 16.1666 24.5C16.1676 24.3716 16.1923 24.2444 16.2395 24.125C16.2395 24.0729 16.2916 24.0416 16.3124 23.9896C16.3398 23.9353 16.3711 23.883 16.4062 23.8333L21.6145 17.5833C21.7124 17.4657 21.8351 17.3712 21.9737 17.3064C22.1123 17.2415 22.2636 17.2081 22.4166 17.2083C22.66 17.2078 22.8958 17.2926 23.0833 17.4479C23.1887 17.5353 23.2759 17.6427 23.3398 17.7639C23.4037 17.8851 23.4431 18.0177 23.4557 18.1542C23.4683 18.2906 23.4538 18.4282 23.4131 18.559C23.3725 18.6899 23.3064 18.8114 23.2187 18.9166L19.4374 23.4583H31.7916C32.0679 23.4583 32.3328 23.5681 32.5282 23.7634C32.7235 23.9588 32.8333 24.2237 32.8333 24.5C32.8333 24.7762 32.7235 25.0412 32.5282 25.2366C32.3328 25.4319 32.0679 25.5416 31.7916 25.5416Z"
                                        fill="#1D4B8B"
                                    />
                                </svg>
                            </button>
                            <button
                                onClick={() => swiperRef.current?.slideNext()}
                                className={`rounded-full border border-slate-300 dark:border-primary ${
                                    disableSwiperNext
                                        ? "bg-slate-100 invisible"
                                        : "border border-slate-300 bg-white visible"
                                } absolute right-[-20px] top-2/4 translate-y-[-50%] z-50`}
                                disabled={disableSwiperNext}
                            >
                                <svg
                                    width="40"
                                    height="40"
                                    viewBox="0 0 50 50"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <circle
                                        cx="25"
                                        cy="25"
                                        r="25"
                                        fill="transparent"
                                    />
                                    <path
                                        d="M18.2084 25.5416H30.5626L26.7813 30.0833C26.6938 30.1886 26.6278 30.3102 26.5873 30.441C26.5467 30.5718 26.5323 30.7093 26.5449 30.8457C26.5702 31.1212 26.704 31.3753 26.9167 31.5521C27.1295 31.7289 27.4037 31.8139 27.6792 31.7885C27.9546 31.7631 28.2087 31.6294 28.3855 31.4166L33.5938 25.1666C33.6289 25.1169 33.6602 25.0647 33.6876 25.0104C33.6876 24.9583 33.7397 24.9271 33.7605 24.875C33.8077 24.7555 33.8324 24.6284 33.8334 24.5C33.8324 24.3716 33.8077 24.2444 33.7605 24.125C33.7605 24.0729 33.7084 24.0416 33.6876 23.9896C33.6602 23.9353 33.6289 23.883 33.5938 23.8333L28.3855 17.5833C28.2876 17.4657 28.1649 17.3712 28.0263 17.3064C27.8877 17.2415 27.7364 17.2081 27.5834 17.2083C27.34 17.2078 27.1042 17.2926 26.9167 17.4479C26.8113 17.5353 26.7241 17.6427 26.6602 17.7639C26.5963 17.8851 26.5569 18.0177 26.5443 18.1542C26.5317 18.2906 26.5462 18.4282 26.5869 18.559C26.6275 18.6899 26.6936 18.8114 26.7813 18.9166L30.5626 23.4583H18.2084C17.9321 23.4583 17.6672 23.5681 17.4718 23.7634C17.2765 23.9588 17.1667 24.2237 17.1667 24.5C17.1667 24.7762 17.2765 25.0412 17.4718 25.2366C17.6672 25.4319 17.9321 25.5416 18.2084 25.5416Z"
                                        fill="#1D4B8B"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                )}
            </section>
        </motion.div>
    );
}

const LastGameCard = ({ games }) => (
    <div className="p-7 bg-white dark:bg-primary_shade shadow-card border border-white dark:border-primary rounded-3xl w-full max-w-md">
        <div className="flex flex-row justify-between items-center mb-5">
            <div>
                <h3 className="text-lg uppercase font-black text-primaryLight">
                    {games && games[0].game_date}
                </h3>
                <p className="text-primaryLight text-xs mt-1">
                    {games && games[0].journey}
                </p>
            </div>
            <ButtonLink
                url="https://www.proballers.com/basketball/game/720424/les-sables-pont-de-cheruy-2023-03-21"
                label="stats"
                bg="bg-accent"
            />
        </div>
        <div className="flex flex-row justify-between items-center mt-5 mx-5">
            <div className="text-center">
                <Image
                    src={
                        games ? games[0].home_image_url : "/static/svg/logo.svg"
                    }
                    alt="team logo"
                    width={45}
                    height={45}
                    className="mx-auto pb-2"
                />
                <p className="text-sm text-center font-bold text-primary dark:text-primaryLighter mb-2 max-w-[100px] uppercase leading-4 min-h-[32px]">
                    {games && games[0].home_name}
                </p>
                <p
                    className={`text-2xl text-center font-black ${
                        games &&
                        parseInt(games[0].home_score) >
                            parseInt(games[0].visitor_score)
                            ? "text-primary dark:text-primaryLighter"
                            : "text-primaryLighter"
                    }`}
                >
                    {games && games[0].home_score}
                </p>
            </div>
            <div className="text-center">
                <Image
                    src={
                        games
                            ? games[0].visitor_image_url
                            : "/static/svg/logo.svg"
                    }
                    alt="team logo"
                    width={45}
                    height={45}
                    className="mx-auto pb-2"
                />

                <p className="text-sm text-center font-bold text-primary  dark:text-primaryLighter mb-2 max-w-[100px] uppercase leading-4 min-h-[32px]">
                    {games && games[0].visitor_name}
                </p>
                <p
                    className={`text-2xl text-center font-black ${
                        games &&
                        parseInt(games[0].visitor_score) >
                            parseInt(games[0].home_score)
                            ? "text-primary"
                            : "text-primaryLight"
                    }`}
                >
                    {games && games[0].visitor_score}
                </p>
            </div>
        </div>
        {/* event location */}
        <div className="flex mt-5">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6 text-primaryLight"
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                />
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                />
            </svg>

            <span className="text-xs capitalize text-primaryLight ml-1 mt-1">
                {games && games[0].game_venue}
            </span>
        </div>
    </div>
);

const UpcomingGames = ({ game, index }) => (
    <div
        key={game.id}
        className="p-7 bg-white border border-slate-300 dark:bg-primary_shade dark:border-primary rounded-3xl w-full min-h-[299px] h-[295px]"
    >
        <div className="flex flex-row justify-between items-center mb-5">
            <div>
                <h3 className="text-lg uppercase font-black text-primary dark:text-primaryLighter">
                    {game.game_date}
                </h3>
                <p className="text-primaryLight text-xs mt-1">{game.journey}</p>
            </div>
            {index === 0 && (
                <ButtonLink
                    url="https://www.lsvb.fr/billetterie"
                    label="billeterie"
                    bg="bg-accent"
                />
            )}
        </div>
        <div className="flex flex-row justify-between items-center">
            <div className="text-center">
                <Image
                    src={game.home_image_url}
                    alt={game.home_name}
                    width={45}
                    height={45}
                    className="mx-auto pb-2"
                />

                <p className="text-sm text-center font-bold text-primary dark:text-primaryLighter uppercase max-w-[100px] leading-4">
                    {game.home_name}
                </p>
            </div>
            <div className="text-center">
                <Image
                    src={game.visitor_image_url}
                    alt={game.visitor_name}
                    width={45}
                    height={45}
                    className="mx-auto pb-2"
                />
                <p className="text-sm text-center font-bold text-primary dark:text-primaryLighter uppercase max-w-[100px] leading-4">
                    {game.visitor_name}
                </p>
            </div>
        </div>

        {/* event location */}
        <div className="flex flex-row justify-start items-center mt-14">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6 text-primaryLight"
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                />
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                />
            </svg>
            <span className="text-xs capitalize text-primaryLight ml-1 mt-1">
                {game.game_venue}
            </span>
        </div>
    </div>
);
