import React from "react";
import Link from "next/link";
import Image from "next/image";

export default function News() {
    return (
        <section className="pt-[4rem] pb-16">
            <section className="container max-w-6xl px-5">
                <h3 className="text-2xl font-black font-mulish text-primary dark:text-primaryLighter uppercase">
                    #lsvbnation.
                </h3>

                <div className="mt-8 flex flex-col md:flex-row gap-5">
                    {/* card */}
                    <div className="min-h-[462px]">
                        <div className="block max-w-sm rounded-lg bg-white shadow-lg dark:bg-primary_shade">
                            <a
                                aria-label="liens vers le blog 1"
                                target="_blank"
                                rel="noopener noreferrer"
                                href="https://actu.fr/pays-de-la-loire/les-sables-d-olonne_85194/un-nouveau-coach-pour-lequipe-premiere-de-les-sables-vendee-basket_59564447.html"
                            >
                                <Image
                                    src="/static/images/blog_new_coach.jpg"
                                    alt="nm1 blog image"
                                    width={960}
                                    height={640}
                                />
                            </a>
                            <div className="p-6 min-h-[247px]">
                                <h4 className="text-sm font-mulish uppercase font-black leading-relaxed text-primary dark:text-white">
                                    Un nouveau coach pour la NM1.
                                </h4>
                                <p className="my-3 text-sm text-primary dark:text-primaryLighter">
                                    Denis Mettay a signé avec le club de basket
                                    des Sables-d'Olonne pour prendre en main
                                    l'équipe première.
                                </p>
                                <Link
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://actu.fr/pays-de-la-loire/les-sables-d-olonne_85194/un-nouveau-coach-pour-lequipe-premiere-de-les-sables-vendee-basket_59564447.html"
                                >
                                    <button
                                        type="button"
                                        className="inline-block rounded-xl bg-accent_blue dark:bg-accent_blue px-6 pt-2.5 pb-2 text-sm font-medium capitalize leading-normal text-white transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                                        data-te-ripple-init
                                        data-te-ripple-color="light"
                                    >
                                        details
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    {/* card */}
                    <div className="">
                        <div className="block max-w-sm rounded-lg bg-white shadow-lg dark:bg-primary_shade">
                            <a
                                aria-label="liens vers le blog 2"
                                target="_blank"
                                rel="noopener noreferrer"
                                href="https://actu.fr/pays-de-la-loire/les-sables-d-olonne_85194/les-sables-dolonne-vendee-basket-doit-embraser-beausejour-ce-soir-pour-le-dernier-match-de-la-saison_59447401.html"
                            >
                                <Image
                                    src="/static/images/blog_dernier_match.jpg"
                                    alt="nm1 blog image"
                                    width={960}
                                    height={640}
                                />
                            </a>
                            <div className="p-6  min-h-[247px]">
                                <h4 className="text-sm font-mulish uppercase font-black leading-relaxed text-primary dark:text-white">
                                    NM1, le dernier match de la saison.
                                </h4>
                                <p className="my-3 text-sm text-primary dark:text-primaryLighter">
                                    Le public de Beauséjour saluera une dernière
                                    fois son équipe malgré une descente sportive
                                    en N2.
                                </p>
                                <Link
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://actu.fr/pays-de-la-loire/les-sables-d-olonne_85194/les-sables-dolonne-vendee-basket-doit-embraser-beausejour-ce-soir-pour-le-dernier-match-de-la-saison_59447401.html"
                                >
                                    <button
                                        type="button"
                                        className="inline-block rounded-xl bg-accent_blue dark:bg-accent_blue px-6 pt-2.5 pb-2 text-sm font-medium capitalize leading-normal text-white transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                                        data-te-ripple-init
                                        data-te-ripple-color="light"
                                    >
                                        details
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    {/* card */}
                    <div className="">
                        <div className="block max-w-sm rounded-lg bg-white shadow-lg dark:bg-primary_shade">
                            <a
                                aria-label="liens vers le blog 3"
                                target="_blank"
                                rel="noopener noreferrer"
                                href="https://actu.fr/pays-de-la-loire/les-sables-d-olonne_85194/les-sables-vendee-basket-la-victoire-a-cergy-pontoise-va-laisser-des-regrets_59315618.html"
                            >
                                <Image
                                    src="/static/images/blog_victoire.jpg"
                                    alt="nm1 blog image"
                                    width={960}
                                    height={640}
                                />
                            </a>
                            <div className="p-6  min-h-[247px]">
                                <h4 className="text-sm font-mulish uppercase font-black leading-relaxed text-primary dark:text-white">
                                    La victoire à Cergy-Pontoise va laisser des
                                    regrets.
                                </h4>
                                <p className="my-3 text-sm text-primary dark:text-primaryLighter">
                                    Le LSVB termine bien sa saison en s'imposant
                                    à Cergy-Pontoise (93-76).
                                </p>
                                <Link
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://actu.fr/pays-de-la-loire/les-sables-d-olonne_85194/les-sables-vendee-basket-la-victoire-a-cergy-pontoise-va-laisser-des-regrets_59315618.html"
                                >
                                    <button
                                        type="button"
                                        className="inline-block rounded-xl bg-accent_blue dark:bg-accent_blue px-6 pt-2.5 pb-2 text-sm font-medium capitalize leading-normal text-white transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                                        data-te-ripple-init
                                        data-te-ripple-color="light"
                                    >
                                        details
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

                {/* switch */}
            </section>
        </section>
    );
}
