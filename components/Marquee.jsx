import React from "react";
import Image from "next/image";
export default function Marquee() {
    return (
        <div className="overflow-hidden">
            <div className="slder-container">
                <div className="slider-content-wrapper inline-block whitespace-nowrap overflow-hidden animate-marquee_custom">
                    {/* block one */}
                    <div className="inline-block ">
                        <div className="whitespace-nowrap overflow-hidden">
                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/region.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>
                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/homkia.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/leclerc.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/city.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/depts.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/region.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/homkia.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>
                        </div>
                    </div>
                    {/* block two */}
                    <div className="inline-block ">
                        <div className="whitespace-nowrap overflow-hidden">
                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/region.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>
                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/homkia.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/leclerc.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/city.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/depts.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/region.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>

                            <div className="item inline-block w-[200px] md:w-[220px] ml-[1rem] md:ml-[5rem]">
                                <Image
                                    src="/static/images/homkia.png"
                                    alt="logo"
                                    width={220}
                                    height={95}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
