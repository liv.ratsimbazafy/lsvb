import { useState } from "react";
import Head from "next/head";
import Topbar from "./navigation/Topbar";
import Drawer from "./Drawer";

export default function Layout({ title, description, children }) {
    let [isOpen, setIsOpen] = useState(false);

    return (
        <>
            <Head>
                <title>{title ? title + " - LSVB N1" : "LSVB N1"}</title>
                <meta name="description" content={description} />
            </Head>
            <div id="main">
                {/* <Topbar isOpen={isOpen} setIsOpen={setIsOpen} /> */}
                <main className="">{children}</main>
            </div>
            {/* <Drawer isOpen={isOpen} setIsOpen={setIsOpen} /> */}
        </>
    );
}
