import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import Link from "next/link";

import { useTheme } from "next-themes";
import { SunIcon, MoonIcon } from "@heroicons/react/solid";

export default function Drawer({ isOpen, setIsOpen, currentRoute }) {
    const { systemTheme, theme, setTheme } = useTheme();
    const [mounted, setMounted] = useState(false);

    const handleThemeChange = () => {
        //if (!mounted) return null;
        const currentTheme = theme === "system" ? systemTheme : theme;
        return currentTheme === "dark" ? (
            <SunIcon
                className="w-7 h-7 text-white dark:text-primary "
                role="button"
                onClick={() => setTheme("light")}
            />
        ) : (
            <MoonIcon
                className="w-7 h-7 text-white dark:text-primary"
                role="button"
                onClick={() => setTheme("dark")}
            />
        );
    };
    return (
        <Transition
            show={isOpen}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
            as={Fragment}
        >
            <Dialog
                open={isOpen}
                onClose={() => setIsOpen(false)}
                className="fixed top-0 left-0 z-[1000] w-full h-full bg-white dark:bg-primary"
            >
                <div className="fixed inset-0 pt-7">
                    <Dialog.Panel className="w-full rounded px-5">
                        <div className="flex flex-row justify-between items-center">
                            <div className="w-12 h-12 rounded-full flex justify-center items-center bg-primary dark:bg-white shadow-xl">
                                {handleThemeChange()}
                            </div>
                            <div
                                role="button"
                                className="w-12 h-12 rounded-full flex justify-center items-center bg-white shadow-xl"
                                onClick={() => setIsOpen(false)}
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 24 24"
                                    fill="currentColor"
                                    className="w-6 h-6 text-primary"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M5.47 5.47a.75.75 0 011.06 0L12 10.94l5.47-5.47a.75.75 0 111.06 1.06L13.06 12l5.47 5.47a.75.75 0 11-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 01-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 010-1.06z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                            </div>
                        </div>
                        {/* <Dialog.Title>Complete your order</Dialog.Title> */}

                        {/* ... */}
                        <div className="flex flex-col mt-16">
                            {navLinks.map((link, i) => (
                                <StyledLink
                                    key={i}
                                    href={link.href}
                                    label={link.label}
                                    className="mr-3 uppercase"
                                    currentRoute={currentRoute}
                                    setIsOpen={setIsOpen}
                                />
                            ))}
                        </div>
                    </Dialog.Panel>
                </div>
            </Dialog>
        </Transition>
    );
}

const StyledLink = ({ href, label, currentRoute, setIsOpen }) => {
    return (
        <Link
            onClick={() => setIsOpen(false)}
            href={href}
            className={`mr-10 uppercase font-bold text-lg ${
                currentRoute === href
                    ? "text-accent dark:text-accent"
                    : "text-primary"
            } dark:text-white hover:text-accent dark:hover:text-accent duration-300 dark:duration-300 py-2 focus:outline-none focus:ring-[transparent]`}
        >
            {label}
        </Link>
    );
};

const navLinks = [
    {
        href: "/",
        label: "accueil",
        targetSeg: "accueil",
    },
    {
        href: "#",
        label: "le club",
        targetSeg: "home",
    },
    {
        href: "/nm1",
        label: "nm1",
        targetSeg: "nm1",
    },
    {
        href: "#",
        label: "equipes",
        targetSeg: "equipes",
    },
    {
        href: "/billetterie",
        label: "billetterie",
        targetSeg: "billetterie",
    },
    {
        href: "#",
        label: "boutique",
        targetSeg: "boutique",
    },
    {
        href: "#",
        label: "lsvb tv",
        targetSeg: "tv",
    },
];
