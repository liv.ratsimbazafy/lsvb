import React, { useState } from "react";
import Image from "next/image";
import { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
// Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

export default function TeamLeaders({ leaders }) {
    const [slideCount, setSlideCount] = useState(1);

    return (
        <div className="relative">
            <Swiper
                className=" relative"
                modules={[Navigation]}
                spaceBetween={20}
                breakpoints={{
                    320: {
                        slidesPerView: 2,
                    },
                    1024: {
                        slidesPerView: 3,
                    },
                }}
                onSwiper={(swiper) => {
                    setSlideCount(1);
                }}
                onSlideChange={(swiper) =>
                    setSlideCount(swiper.activeIndex + 1)
                }
            >
                {leaders &&
                    leaders.map((leader, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <div
                                    key={index}
                                    className="mt-12 text-center bg-white dark:bg-primary_shade border border-slate-200 dark:border-primary rounded-xl min-w-[140px] min-h-[150px] relative"
                                >
                                    <Image
                                        blurDataURL="/static/images/trapp.png"
                                        src={leader.image_url}
                                        alt={leader.player}
                                        width={70}
                                        height={70}
                                        className="mx-auto absolute top-[-35px] left-[50%] translate-x-[-50%] rounded-full drop-shadow-xl"
                                    />
                                    <div className="pt-12 pb-3">
                                        <p className="uppercase text-primary dark:text-primaryLighter text-sm font-black">
                                            {leader.label}
                                        </p>
                                        <p className="uppercase text-primary dark:text-primaryLighter font-black text-xl mb-2">
                                            {leader.stats}
                                        </p>
                                        <p className="text-sm text-primaryLight capitalize">
                                            {leader.player}
                                        </p>
                                    </div>
                                </div>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
            <p className="mx-auto text-center mt-8 p-2 rounded-full font-bold border border-slate-300 bg-white text-slate-400 dark:bg-primary_shade dark:text-primaryLighter text-sm max-w-[80px]">
                {slideCount} / {leaders.length - 1}
            </p>
        </div>
    );
}
