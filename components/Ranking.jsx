import React, { useState } from "react";
import RankingTable from "./RankingTable";

export default function Ranking({ ranking }) {
    const [grpA, setGrpA] = useState(false);
    const [grpB, setGrpB] = useState(false);
    const [grpC, setGrpC] = useState(true);
    const [heading, setHeading] = useState(
        "L’équipe classée à la première place est sacrée champion NM1, les autres équipes sont qualifiées pour les playoffs."
    );

    const [standing, setStanding] = useState(
        ranking.filter((rank) => {
            return rank.group === "C";
        })
    );

    const sortTeamPoints = (group) => {
        let tmp = ranking.filter((rank) => {
            return rank.group === group;
        });
        tmp = tmp.sort((a, b) => {
            return parseInt(b.points) - parseInt(a.points);
        });
        return tmp;
    };

    const handleTabs = (e) => {
        let tgt = e.currentTarget.dataset.name;
        switch (tgt) {
            case "grpA":
                setGrpA(true);
                setGrpB(false);
                setGrpC(false);
                setHeading(
                    "L’équipe classée à la première place est sacrée champion NM1, les autres équipes sont qualifiées pour les playoffs."
                );
                setStanding(
                    ranking.filter((rank) => {
                        return rank.group === "A";
                    })
                );
                break;
            case "grpB":
                setGrpB(true);
                setGrpA(false);
                setGrpC(false);
                setHeading(
                    "Les équipes classées de la 1ère à la 7e place sont qualifiées pour les playoffs."
                );
                setStanding(sortTeamPoints("B"));
                break;
            case "grpC":
                setGrpC(true);
                setGrpB(false);
                setGrpA(false);
                setHeading(
                    "Les équipes classées aux 4 dernières places (hors PFBB) sont reléguées en NM2."
                );
                setStanding(sortTeamPoints("C"));
                break;
            default:
                setGrpA(true);
                break;
        }
    };

    return (
        <section className="pt-[8rem] pb-16">
            <section className="container max-w-6xl px-5">
                <h3 className="text-2xl font-black font-mulish text-primary dark:text-primaryLighter uppercase">
                    #classement NM1.
                </h3>

                {/* switch */}
                <div className="flex flex-row justify-start items-center gap-x-3 py-5">
                    <button
                        className={`px-4 py-2.5 ${
                            grpA
                                ? "bg-accent_blue text-white"
                                : "border border-slate-300 bg-white text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                        } text-sm font-bold capitalize rounded-xl`}
                        onClick={handleTabs}
                        data-name="grpA"
                    >
                        poule A
                    </button>
                    <button
                        className={`px-4 py-2.5 ${
                            grpB
                                ? "bg-accent_blue text-white"
                                : "border border-slate-300 bg-white text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                        } text-sm font-bold capitalize rounded-xl`}
                        onClick={handleTabs}
                        data-name="grpB"
                    >
                        poule B
                    </button>
                    <button
                        className={`px-4 py-2.5 ${
                            grpC
                                ? "bg-accent_blue text-white"
                                : "border border-slate-300 bg-white text-primary dark:text-primaryLighter dark:bg-primary_shade dark:border-primary_shade"
                        } text-sm font-bold capitalize rounded-xl`}
                        onClick={handleTabs}
                        data-name="grpC"
                    >
                        poule C
                    </button>
                </div>

                <RankingTable standing={standing} heading={heading} />
            </section>
        </section>
    );
}
