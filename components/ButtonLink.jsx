import React from "react";
import Link from "next/link";

export default function ButtonLink({ url, label, bg }) {
    return (
        <Link
            href={url}
            aria-label={label}
            rel="noreferrer"
            target="_blank"
            className={`py-2 px-3 ${bg} text-sm text-white font-bold capitalize rounded-xl`}
        >
            {label}
        </Link>
    );
}
