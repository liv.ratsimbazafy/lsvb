import React from "react";
import { classNames } from "@/helpers";

export function Button({ children, className, ...rest }) {
    return (
        <button
            type="button"
            className={classNames(
                "relative inline-flex items-center px-4 py-2 border border-slate-300 dark:border-primary_shade text-sm font-medium rounded-full text-slate-300 bg-white dark:bg-primary_shade",
                className
            )}
            {...rest}
        >
            {children}
        </button>
    );
}

export function PageButton({ children, className, ...rest }) {
    return (
        <button
            type="button"
            className={classNames(
                "relative inline-flex items-center px-2 py-2 border border-slate-300 bg-white text-sm text-slate-300 dark:border-primary_shade dark:bg-primary_shade dark:hover:bg-primary",
                className
            )}
            {...rest}
        >
            {children}
        </button>
    );
}
