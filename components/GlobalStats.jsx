import React, { useState } from "react";
import Image from "next/image";

import { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

// Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

export default function GlobalStats({ stats }) {
    const [slideCount, setSlideCount] = useState(1);

    return (
        <div className="relative">
            <Swiper
                className=" relative"
                //modules={[Navigation]}
                spaceBetween={20}
                breakpoints={{
                    320: {
                        slidesPerView: 2,
                    },
                    1024: {
                        slidesPerView: 3,
                    },
                }}
                onSwiper={(swiper) => {
                    setSlideCount(1);
                }}
                onSlideChange={(swiper) =>
                    setSlideCount(swiper.activeIndex + 1)
                }
            >
                {stats.map((item, index) => {
                    let img_url = "";
                    if (index === 0) img_url = "/static/svg/badge.svg";
                    else if (index === 1) img_url = "/static/svg/shoot.svg";
                    else img_url = "/static/svg/basketball-hoop.svg";

                    return (
                        <SwiperSlide key={index}>
                            <div className="mt-12 bg-white text-center rounded-xl border border-slate-300 min-w-[140px] min-h-[150px] relative dark:bg-primary_shade dark:border-primary">
                                <Image
                                    blurDataURL="/static/svg/hoop.svg"
                                    src={img_url}
                                    alt="logo"
                                    width={70}
                                    height={70}
                                    className="mx-auto absolute top-[-35px] left-[50%] translate-x-[-50%] rounded-full drop-shadow-xl bg-white p-3"
                                />
                                <div className="pt-12 pb-3 text-primary dark:text-primaryLighter">
                                    <p className="uppercasetext-sm font-black">
                                        {item.title}
                                    </p>
                                    <p className="uppercase font-black text-xl my-2">
                                        {item.stats}
                                    </p>
                                </div>
                            </div>
                        </SwiperSlide>
                    );
                })}
            </Swiper>
            <p className="mx-auto text-center mt-8 p-2 rounded-xl font-bold border border-slate-300 bg-white text-primary dark:bg-primary_shade dark:text-primaryLighter dark:border-primary text-sm max-w-[80px]">
                {slideCount} / {stats.length - 1}
            </p>
        </div>
    );
}
