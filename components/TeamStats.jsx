import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import ButtonLink from "./ButtonLink";

export default function TeamStats({ games }) {
    //console.log(games);

    return (
        <section className="py-[7rem] lg:pt-[15rem] font-mulish">
            <div className="container px-5 md:px-12 lg:px-0">
                <div className="flex flex-col gap-y-[5rem] lg:flex-row lg:gap-y-0 lg:gap-x-[10rem]">
                    {/* Left card */}
                    <div className="w-full lg:w-1/3">
                        <div className="flex flex-row justify-between items-center mb-5 lg:mb-[4rem]">
                            <h3 className="text-2xl xl:text-3xl uppercase font-black text-primary ">
                                dernier résultats.
                            </h3>
                            {/* <ButtonLink
                                url="#"
                                label="statistiques"
                                bg="bg-accent"
                            /> */}
                        </div>
                        {/* card */}
                        <div className="p-7 bg-whiter shadow-card rounded-3xl w-full">
                            <div className="flex flex-row justify-between items-center mb-5">
                                <div>
                                    <h3 className="text-lg uppercase font-black text-primaryLight">
                                        {games && games[0].game_date}
                                    </h3>
                                    <p className="text-primaryLight text-sm">
                                        {games && games[0].journey}
                                    </p>
                                </div>
                            </div>
                            <div className="flex flex-row justify-between items-center mt-5 mx-5">
                                <div className="text-center">
                                    <Image
                                        //src="/static/svg/logo.svg"
                                        src={
                                            games
                                                ? games[0].home_image_url
                                                : "/static/svg/logo.svg"
                                        }
                                        alt="team logo"
                                        width={60}
                                        height={60}
                                        className="mx-auto pb-2"
                                    />
                                    <p className="text-sm text-center font-bold text-primary mb-5 max-w-[100px] uppercase leading-4 min-h-[32px]">
                                        {games && games[0].home_name}
                                    </p>
                                    <p
                                        className={`text-2xl text-center font-black ${
                                            games &&
                                            parseInt(games[0].home_score) >
                                                parseInt(games[0].visitor_score)
                                                ? "text-primary"
                                                : "text-primaryLighter"
                                        }`}
                                    >
                                        {games && games[0].home_score}
                                    </p>
                                </div>
                                <div className="text-center">
                                    <Image
                                        //src="/static/images/logo_vitre.png"
                                        src={
                                            games
                                                ? games[0].visitor_image_url
                                                : "/static/svg/logo.svg"
                                        }
                                        alt="team logo"
                                        width={60}
                                        height={60}
                                        className="mx-auto pb-2"
                                    />

                                    <p className="text-sm text-center font-bold text-primary mb-5 max-w-[100px] uppercase leading-4 min-h-[32px]">
                                        {games && games[0].visitor_name}
                                    </p>
                                    <p
                                        className={`text-2xl text-center font-black ${
                                            games &&
                                            parseInt(games[0].visitor_score) >
                                                parseInt(games[0].home_score)
                                                ? "text-primary"
                                                : "text-primaryLight"
                                        }`}
                                    >
                                        {games && games[0].visitor_score}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Right card */}
                    <div className="w-full lg:w-2/3">
                        {/* header */}
                        <div className="flex flex-row justify-between items-center mb-5 lg:mb-[3rem]">
                            <h3 className="text-2xl xl:text-3xl uppercase font-black text-primary">
                                à venir.
                            </h3>
                            <ButtonLink
                                url="#"
                                label="calendrier"
                                bg="bg-accent"
                            />
                        </div>

                        <div className="flex flex-col gap-y-10 md:flex-row md:gap-y-0 md:gap-x-10">
                            {games &&
                                games.slice(1, 3).map((game) => (
                                    <div
                                        key={game.id}
                                        className="p-7 bg-primaryLighter shadow-card rounded-3xl w-full md:w-1/2 h-[295px]md:max-w-[370px]"
                                    >
                                        <div className="flex flex-row justify-between items-center mb-5">
                                            <div>
                                                <h3 className="text-lg uppercase font-black text-primary">
                                                    {game.game_date}
                                                </h3>
                                                <p className="text-primaryLight text-sm">
                                                    {game.journey}
                                                </p>
                                            </div>
                                        </div>
                                        <div className="flex flex-row justify-between items-center">
                                            <div className="text-center">
                                                <Image
                                                    src={game.home_image_url}
                                                    alt={game.home_name}
                                                    width={55}
                                                    height={55}
                                                    className="mx-auto pb-2"
                                                />

                                                <p className="text-sm text-center font-bold text-primary uppercase max-w-[100px] leading-4">
                                                    {game.home_name}
                                                </p>
                                            </div>
                                            <div className="text-center">
                                                <Image
                                                    src={game.visitor_image_url}
                                                    alt={game.visitor_name}
                                                    width={55}
                                                    height={55}
                                                    className="mx-auto pb-2"
                                                />
                                                <p className="text-sm text-center font-bold text-primary uppercase max-w-[100px] leading-4">
                                                    {game.visitor_name}
                                                </p>
                                            </div>
                                        </div>

                                        {/* event location */}
                                        <div className="flex mt-10">
                                            <Image
                                                src="/static/svg/location_logo.svg"
                                                alt="location logo"
                                                width={15}
                                                height={20}
                                            />
                                            <span className="text-sm capitalize text-primaryLight ml-1">
                                                {game.game_venue}
                                            </span>
                                        </div>
                                    </div>
                                ))}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
