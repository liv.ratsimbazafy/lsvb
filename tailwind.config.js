/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./app/**/*.{js,ts,jsx,tsx}",
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    darkMode: "class",
    theme: {
        // screens: {
        //     sm: "480px",
        //     md: "780px",
        //     lg: "976px",
        //     xl: "1440px",
        // },
        container: {
            center: true,
        },
        extend: {
            fontFamily: {
                mulish: ["Mulish", "sans-serif"],
            },
            colors: {
                primary: "#1D4B8B",
                primaryLighter: "#D8E7FC",
                primaryLight: "#8EA5C5",
                primary_shade: "#1F5298",
                accent: "#FF9F08",
                accent_blue: "#1F72E5",
                accentShade: "#EA8710",
            },
            boxShadow: {
                custom: "0px 70px 50px rgba(0, 0, 0, 0.15)",
                card: "0px 4px 50px rgba(0, 0, 0, 0.1)",
            },
            keyframes: {
                marquee_lg: {
                    "0%": { transform: "translateX(0)" },
                    "100%": { transform: "translateX(-1435px)" },
                },
                marquee_custom: {
                    from: { transform: "translate3d(0,0,0)" },
                    to: { transform: "translate3d(-50%,0,0)" },
                },
            },
            animation: {
                marquee_lg: "marquee_lg 25s linear infinite",
                marquee_custom: "marquee_custom 25s linear infinite",
            },
            backgroundImage: {
                "hero-bg": "url('/hero-bg.png')",
                "dark-hero-bg": "url('/dark-hero-bg.png')",
                "sub-hero-bg":
                    "linear-gradient(to right, rgba(0,0,0,.5), rgba(0,0,0,.5)), url('/team.png')",
            },
        },
    },
    plugins: [
        require("tailwind-scrollbar-hide"),
        require("@tailwindcss/forms"),
    ],
};
