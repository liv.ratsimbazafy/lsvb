import "@/styles/globals.css";
import React, { useEffect, useState } from "react";
import { ThemeProvider } from "next-themes";
import Topbar from "@/components/navigation/Topbar";
import { useRouter } from "next/router";
import { AnimatePresence } from "framer-motion";
import Drawer from "@/components/Drawer";

export default function App({ Component, pageProps }) {
    const [isOpen, setIsOpen] = useState(false);
    // router
    const router = useRouter();
    //console.log(router);
    // Hide splash screen when we are server side
    useEffect(() => {
        if (typeof window !== "undefined") {
            const splashScreen = document.getElementById("splash_screen");
            const main = document.getElementById("main");
            // if (splashScreen) {
            //     setTimeout(() => {
            //         splashScreen.style.transform = "translateY(-100vh)";
            //     }, 1500);
            //     main.setAttribute("style", "opacity: 1; visibility: visible");
            // }
        }
    }, []);
    //return <Component {...pageProps} />
    return (
        <AnimatePresence mode="wait">
            <ThemeProvider enableSystem={true} attribute="class">
                <Topbar
                    isOpen={isOpen}
                    setIsOpen={setIsOpen}
                    currentRoute={router.pathname}
                />
                <Component {...pageProps} key={router.pathname} />
                <Drawer
                    isOpen={isOpen}
                    setIsOpen={setIsOpen}
                    currentRoute={router.pathname}
                />
            </ThemeProvider>
        </AnimatePresence>
    );
}
