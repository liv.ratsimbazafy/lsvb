import React, { useMemo } from "react";
import Layout from "@/components/Layout";
import Hero from "@/components/Hero";
import axios from "axios";
import Calendar from "@/components/Calendar";
import Team from "@/components/Team";
import Ranking from "@/components/Ranking";
import StatChart from "@/components/StatChart";
import PlayerStats from "@/components/PlayerStats";
import { motion } from "framer-motion";
import News from "@/components/News";

export default function Home({ api_data }) {
    const columns = useMemo(
        () => [
            {
                Header: "Joueur",
                accessor: "Joueur",
            },
            {
                Header: "Pts",
                accessor: "pts",
            },
            {
                Header: "pd",
                accessor: "pds",
            },
            {
                Header: "reb",
                accessor: "reb",
            },
            {
                Header: "Eval",
                accessor: "eval",
            },
            {
                Header: "2Pts",
                accessor: "2r-2t",
            },
            {
                Header: "3Pts",
                accessor: "3r-3t",
            },
            {
                Header: "lfr",
                accessor: "lf%",
            },
            {
                Header: "Mj",
                accessor: "MJ",
            },
            {
                Header: "Tps.j",
                accessor: "min",
            },
            {
                Header: "Total",
                accessor: "t%",
            },
            {
                Header: "+/-",
                accessor: "+/-",
            },
        ],
        []
    );

    const data = React.useMemo(() => getData(), []);

    return (
        <Layout
            title="Accueil"
            description={"Les sables vendée basket équipe 1 N1 basketball ffbb"}
        >
            <Hero />
            {/* <Marquee /> */}
            {/* <TeamStats games={data.games}/> */}
            <Calendar games={api_data.games} />
            <Team
                leaders={api_data.leaders}
                roster={api_data.roster}
                stats={api_data.team_stat}
                players_data={api_data.player_stat}
                columns={columns}
            />
            <Ranking ranking={api_data.standings} />
            <News />
            {/* <StatChart /> */}
            {/* <PlayerStats data={api_data.player_stat} columns={columns} /> */}
        </Layout>
    );
}

export async function getServerSideProps(context) {
    const res = await axios.get("https://tsolay.lemurianroots.com/lsvb");
    //console.log(res.data);
    return {
        props: {
            api_data: res.data.data,
        },
    };
}

// ============= Testing REACT TABLE
const getData = () => {
    const data = [
        {
            name: "Jane Cooper",
            email: "jane.cooper@example.com",
            title: "Regional Paradigm Technician",
            department: "Optimization",
            status: "Active",
            role: "Admin",
            imgUrl: "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
        },
        {
            name: "Cody Fisher",
            email: "cody.fisher@example.com",
            title: "Product Directives Officer",
            department: "Intranet",
            status: "Active",
            role: "Owner",
            imgUrl: "https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
        },
        {
            name: "Esther Howard",
            email: "esther.howard@example.com",
            title: "Forward Response Developer",
            department: "Directives",
            status: "Active",
            role: "Member",
            imgUrl: "https://images.unsplash.com/photo-1520813792240-56fc4a3765a7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
        },
        {
            name: "Jenny Wilson",
            email: "jenny.wilson@example.com",
            title: "Central Security Manager",
            department: "Program",
            status: "Active",
            role: "Member",
            imgUrl: "https://images.unsplash.com/photo-1498551172505-8ee7ad69f235?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
        },
        {
            name: "Kristin Watson",
            email: "kristin.watson@example.com",
            title: "Lean Implementation Liaison",
            department: "Mobility",
            status: "Active",
            role: "Admin",
            imgUrl: "https://images.unsplash.com/photo-1532417344469-368f9ae6d187?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
        },
        {
            name: "Cameron Williamson",
            email: "cameron.williamson@example.com",
            title: "Internal Applications Engineer",
            department: "Security",
            status: "Active",
            role: "Member",
            imgUrl: "https://images.unsplash.com/photo-1566492031773-4f4e44671857?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60",
        },
    ];

    return [...data, ...data, ...data];
};
