// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const players = [
    {
        id: 1,
        name: "tom",
        img: "/static/images/city.png",
    },
    {
        id: 2,
        name: "bill",
        img: "/static/images/city.png",
    },
    {
        id: 3,
        name: "jane",
        img: "/static/images/city.png",
    },
    {
        id: 4,
        name: "dale",
        img: "/static/images/city.png",
    },
];
export default function handler(req, res) {
    res.status(200).json(players);
}
