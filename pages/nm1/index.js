import React from "react";
//useSWR allows the use of SWR inside function components
import useSWR from "swr";
import Layout from "@/components/Layout";
import Image from "next/image";
import Link from "next/link";
import { motion } from "framer-motion";

//Write a fetcher function to wrap the native fetch function and return the result of a call to url in json format
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function index() {
    const { data, error } = useSWR("/api/players", fetcher);
    //console.log(data);

    // handle error state
    if (error) return <div>Failed to load</div>;
    //Handle the loading state
    if (!data) return <div>Loading...</div>;

    // animation
    const easing = [0.6, -0.05, 0.01, 0.99];
    const fadeInUp = {
        initial: {
            y: 60,
            opacity: 0,
        },
        animate: {
            y: 0,
            opacity: 1,
            transition: {
                duration: 2,
                ease: easing,
            },
        },
    };
    const stagger = {
        animate: {
            transition: {
                staggerChildren: 0.1,
                delay: 1.5,
            },
        },
    };

    return (
        // <Layout
        //     title="NM1"
        //     description={
        //         "Équipe Les sables vendée basket équipe 1 N1 basketball ffbb"
        //     }
        // >
        <>
            <motion.div
                initial="initial"
                animate="animate"
                exit={{ opacity: 0 }}
            >
                <div className="container max-w-6xl px-5">
                    <div className="pt-[8rem] lg:pt-[12rem]">
                        <div className="banner w-full h-[050px] lg:h-[150px] flex justify-between items-center ">
                            {/* heading */}
                            <div className="flex justify-between items-center w-full">
                                <h3 className="text-primary dark:text-primaryLighter font-bold uppercase px-3 text-4xl">
                                    effectif
                                </h3>
                                <Link href="/" className="inline-block  bg-slate-100 rounded-full">
                                    <svg
                                        width="40"
                                        height="40"
                                        viewBox="0 0 50 50"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <circle
                                            cx="25"
                                            cy="25"
                                            r="25"
                                            transform="matrix(-1 0 0 1 50 0)"
                                            fill="none"
                                        />
                                        <path
                                            d="M31.7916 25.5416H19.4374L23.2187 30.0833C23.3062 30.1886 23.3722 30.3102 23.4127 30.441C23.4533 30.5718 23.4677 30.7093 23.4551 30.8457C23.4298 31.1212 23.296 31.3753 23.0833 31.5521C22.8705 31.7289 22.5963 31.8139 22.3208 31.7885C22.0454 31.7631 21.7913 31.6294 21.6145 31.4166L16.4062 25.1666C16.3711 25.1169 16.3398 25.0647 16.3124 25.0104C16.3124 24.9583 16.2603 24.9271 16.2395 24.875C16.1923 24.7555 16.1676 24.6284 16.1666 24.5C16.1676 24.3716 16.1923 24.2444 16.2395 24.125C16.2395 24.0729 16.2916 24.0416 16.3124 23.9896C16.3398 23.9353 16.3711 23.883 16.4062 23.8333L21.6145 17.5833C21.7124 17.4657 21.8351 17.3712 21.9737 17.3064C22.1123 17.2415 22.2636 17.2081 22.4166 17.2083C22.66 17.2078 22.8958 17.2926 23.0833 17.4479C23.1887 17.5353 23.2759 17.6427 23.3398 17.7639C23.4037 17.8851 23.4431 18.0177 23.4557 18.1542C23.4683 18.2906 23.4538 18.4282 23.4131 18.559C23.3725 18.6899 23.3064 18.8114 23.2187 18.9166L19.4374 23.4583H31.7916C32.0679 23.4583 32.3328 23.5681 32.5282 23.7634C32.7235 23.9588 32.8333 24.2237 32.8333 24.5C32.8333 24.7762 32.7235 25.0412 32.5282 25.2366C32.3328 25.4319 32.0679 25.5416 31.7916 25.5416Z"
                                            fill="#1D4B8B"
                                        />
                                    </svg>
                                </Link>
                            </div>
                        </div>

                        <motion.div
                            variants={stagger}
                            className="grid grid-cols-2 md:grid-cols-4 gap-x-5 md:gap-x-16 gap-y-10 my-12"
                        >
                            {data.players.map((item) => {
                                return (
                                    <Link
                                        href={`/nm1/${item.slug}`}
                                        key={item.id}
                                    >
                                        <motion.div
                                            className="mt-2 grow relative"
                                            variants={fadeInUp}
                                        >
                                            <Image
                                                priority
                                                src={item.img}
                                                width={500}
                                                height={700}
                                                alt={item.lastname}
                                            />
                                            <div className="absolute bottom-0 left-[50%] translate-x-[-50%] w-full py-1 md:py-3 text-white font-bold bg-primary dark:bg-accent_blue dark:text-white -skew-x-[21deg] rounded-xl flex items-center">
                                                <span className="skew-x-[21deg] inline-block pl-3 text-2xl">
                                                    {item.jersey}
                                                </span>
                                                <div className="skew-x-[21deg] inline-block ml-3">
                                                    <p className="text-sm uppercase">
                                                        {item.firstname.charAt(
                                                            0
                                                        )}
                                                        . {item.lastname}
                                                    </p>
                                                   
                                                </div>
                                            </div>
                                        </motion.div>
                                    </Link>
                                );
                            })}
                        </motion.div>
                    </div>
                </div>
            </motion.div>
        </>
        // </Layout>
    );
}

const Breadcrumb = () => (
    <nav className="flex pl-3 mt-3" aria-label="Breadcrumb">
        <ol className="inline-flex items-center space-x-1 md:space-x-3">
            <li className="inline-flex items-center capitalize">
                <Link
                    href="/"
                    className="inline-flex items-center text-sm font-medium text-primary hover:text-blue-400 dark:text-gray-400 dark:hover:text-primary_shade"
                >
                    accueil
                </Link>
            </li>
            <li>
                <div className="flex items-center">
                    /
                    <a
                        href="#"
                        className="ml-1 text-sm font-medium text-primary hover:text-blue-600 md:ml-2 dark:text-gray-400 dark:hover:text-white"
                    >
                        effectif
                    </a>
                </div>
            </li>
        </ol>
    </nav>
);
